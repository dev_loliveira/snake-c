

#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_native_dialog.h>

#include <iostream>
#include <vector>
#include <map>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

using namespace :: std;

ALLEGRO_DISPLAY      *display          = NULL;
ALLEGRO_TIMER        *timer            = NULL;
ALLEGRO_EVENT_QUEUE  *event_queue      = NULL;
ALLEGRO_FONT         *font             = NULL;

int SCREEN_W      = 840;
int SCREEN_H      = 640;

const float FPS = 60.0;

#include "src/String.cpp"
#include "src/Keys.h"
#include "src/Color.h"
#include "src/Snake.cpp"

#define BACKGROUND_COLOR        WHITE



int main(int argc, char** argv)
{
    char                        key_char[100];
    
    bool                        exit           = false,
                                redraw         = true;
    
    ALLEGRO_MONITOR_INFO        info;
    
    Snake                       snake;
    
    srand (time(NULL));
    
    if(!al_init())
    {
        fprintf(stderr, "Failed to initialize Allegro!\n");
        return -1;
    }
    
    al_init_font_addon();
    al_init_ttf_addon();
    al_init_primitives_addon();
    
    font   = al_load_ttf_font("resource/Font/font02.ttf", 100, 0);
    if(font == NULL)
    {
        fprintf(stderr, "Failed to load font!\n");
        return -1;
    }
    
    al_get_monitor_info(0, &info); 
    //SCREEN_W = info.x2 - info.x1; /* Assume this is 1366 */ 
    //SCREEN_H = info.y2 - info.y1; /* Assume this is 768 */ 
    SCREEN_H = SCREEN_W = 300;
    
    display = al_create_display(SCREEN_W, SCREEN_H);
    if(!display)
    {
        fprintf(stderr, "Failed to create display!\n");
        return -1;
    }
    al_set_target_bitmap (al_get_backbuffer(display));

    timer = al_create_timer (1.0 / FPS);
    if(!timer)
    {
        fprintf(stderr, "Failed to create timer!\n");
        al_destroy_display(display);
        return -1;
    }
    
    event_queue = al_create_event_queue();
    if(!event_queue)
    {
        fprintf(stderr, "Failed to create event queue!\n");
        al_destroy_timer(timer);
        al_destroy_display(display);
        return -1;
    }

    al_install_mouse();
    al_install_keyboard();

    al_register_event_source(event_queue, al_get_display_event_source(display));
    al_register_event_source(event_queue, al_get_timer_event_source(timer));

    if( al_is_keyboard_installed() )
      al_register_event_source(event_queue, al_get_keyboard_event_source());

    if( al_is_mouse_installed() )
      al_register_event_source(event_queue, al_get_mouse_event_source());
    
    //al_hide_mouse_cursor(display);
    
    snake.init();

    al_clear_to_color(BACKGROUND_COLOR);

    al_flip_display();
    
    al_start_timer(timer);

    while(!exit)
    {
        ALLEGRO_EVENT ev;

        al_wait_for_event(event_queue, &ev);

        switch(ev.type)
        {
          case ALLEGRO_EVENT_DISPLAY_CLOSE:
            exit = true;
            break;
        
          case ALLEGRO_EVENT_TIMER:
            if(ev.timer.source == timer)
              redraw = true;
            
            break;
        
          case KEY_DOWN:
            strcpy(key_char, al_keycode_to_name(ev.keyboard.keycode));
        
            if( StringCompareNoCase(key_char, "escape") )
              exit = true;
              
            else if( StringCompareNoCase(key_char, "enter") )
            {
                 al_show_native_message_box(
                     display, "teste", 
                     "NULL", toString (snake.foodX),
                     NULL, 0);
                     
                 al_show_native_message_box(
                     display, "teste", 
                     "NULL", toString (snake.foodY),
                     NULL, 0);
            }
             
            else if( StringCompareNoCase(key_char, "down") )
            {
              if(snake.direction != 2)
                  snake.direction = 3;
            }
          
            else if( StringCompareNoCase(key_char, "up") )
            {
              if(snake.direction != 3)
                  snake.direction = 2;
            }
        
            else if( StringCompareNoCase(key_char, "right") )
            {
              if(snake.direction != 0)
                  snake.direction = 1;
            }
           
            else if( StringCompareNoCase(key_char, "left") )
            {
              if(snake.direction != 1)
                  snake.direction = 0;
            }
            
            else if( StringCompareNoCase(key_char, "u") )
              snake.spd++;
              
            else if( StringCompareNoCase(key_char, "d") )
              snake.spd--;
            
          break;
      
      /**************************
          Soltou uma tecla
      ***************************/
          case KEY_UP:
            strcpy(key_char, al_keycode_to_name(ev.keyboard.keycode));
            break;
          
          case ALLEGRO_EVENT_MOUSE_AXES:/* ev.mouse.x */
            break;
        }
        
        if(redraw && al_is_event_queue_empty(event_queue))
        {
            redraw = false;

            al_clear_to_color(BACKGROUND_COLOR);
            
            snake.print();
            snake.move();
            
            al_flip_display();
        }
    }

    al_destroy_timer(timer);
    al_destroy_display(display);
    al_destroy_event_queue(event_queue);

    return 0;
}

