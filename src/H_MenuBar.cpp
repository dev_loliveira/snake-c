

#ifndef H_MENU_BAR_CPP
#define H_MENU_BAR_CPP

#include "H_MenuBar.h"

int H_MenuBar :: init(ALLEGRO_DISPLAY *display2, ALLEGRO_FONT *font2, long int SCREEN_W, long int SCREEN_H, vector<string>menus)
{
	menuW   = SCREEN_W;
	menuH   = (SCREEN_H)/10;
	options = menus;
	display = display2;
	font = font2;
}

int H_MenuBar :: print(long int mx, long int my)
{
	volatile int      x = 0,
	                  x_next;
	static int        y = menuH / 2;
	
	al_draw_filled_rectangle(
	    0, 0, 
	    menuW, menuH, 
	    WHITE);
	
	for(i = options.begin();i != options.end();i++)
	{
		x_next = al_get_text_width(font, (*i).c_str());
		
	    al_draw_text(
	        font,
	        BLACK, x, y, 0,
	        (*i).c_str());
	        
	    if (my >= 0 && my <= menuH)
	    {
			if (mx >= x && mx <= x_next)
			    al_draw_rectangle(
			        x, y, 
	                x_next, menuH, 
	                BLACK, 1);
		}
	        
		x += x_next;
	}
}

#endif
