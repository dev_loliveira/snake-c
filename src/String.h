
#ifndef STRING_H
#define STRING_H
/*******************************************
  Header que possui alguns metodo que
  auxiliam a manipular string
*******************************************/


char* toString(long int n);

char* reverse(char *str);

char *itoa(int n, int actualBase, int base);

unsigned int StringLengh(char *str);

bool StringCompare(char *str, char *str2);

bool StringCompareNoCase(char *str, char *str2);

void StringCpy(char *str, char *str2);

#endif
