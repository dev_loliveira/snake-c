

#ifndef H_MENU_BAR_H
#define H_MENU_BAR_H

class H_MenuBar
{
	/*
	 * Atributes
	 */
	 long int            menuW,
	                     menuH,
                             fontW, fontH, font_separation;

     vector <string>     options;
     vector <string> :: iterator i;
     
     ALLEGRO_DISPLAY    *display;
     
     ALLEGRO_FONT       *font;
	 
	/*
	 * Functions
	 */
    public:
        int init(ALLEGRO_DISPLAY *display, ALLEGRO_FONT *font, long int SCREEN_W, long int SCREEN_H, vector<string>menus);
	    int print(long int mx, long int my);
};

#endif
