

#ifndef SNAKE_CPP
#define SNAKE_CPP

#define SNAKE_INITIAL_BODY   15
#define SNAKE_SQR_W          2
#define SNAKE_SQR_H          2
#define SNAKE_COLOR          BLACK
#define MESSAGE_COLOR        BLACK
#define FOOD_W               10
#define FOOD_H               10
#define FOOD_COLOR           BLACK
#define FOOD_GAIN            10

#include "Sound.cpp"

class Snake
{
  private:
      Sound                          bck_music;
      
  public:
      long int                       x, y,
                                     foodX, foodY,
                                     pts;
      
      short int                      direction,
                                     spd;
      
      struct BODY
      {
             long int  x, y;
      };
      vector <BODY>  body;
      
      /* Metodos */
      int init();
      int print();
      int move();
      int createFood();
      int GameOver();
};

int Snake :: createFood()
{
    bool         colision = false;
    
    vector <BODY> :: iterator i2;
    
    do
    {
           foodX = rand() % (SCREEN_W - FOOD_W);
           foodY = rand() % (SCREEN_H - FOOD_H) + al_get_font_line_height(font);
           
           colision = false;
           
           for (i2 = body.begin();i2 != body.end();i2++)
           {
               if(foodX >= i2->x && foodX <= i2->x + SNAKE_SQR_W &&
                  foodY >= i2->y && foodY <= i2->y + SNAKE_SQR_H)
                    colision = true;
           }
           
    } while (colision);
}

int Snake :: GameOver()
{
    string            msg;
    
    bck_music.stopAll ();
    
    msg.append ("Terminou com: ");
    msg.append (toString(pts));
    
    al_clear_to_color (WHITE);
    
    al_flip_display ();
    
    al_draw_text (
        font, 
        MESSAGE_COLOR, 
        SCREEN_W/2 - (al_get_text_width (font, msg.c_str())/2), 
        SCREEN_H/2, 
        0, 
        msg.c_str());
    
    al_flip_display ();
    
    al_rest (2);
    
    exit (1);
}

int Snake :: init()
{
    short int    i;
    long int     x = SCREEN_W/2,
                 y = SCREEN_H/2;
    
    BODY        *ptr_body, 
                 aux_body;
    
    bool         colision = false;
                 
    vector <BODY> :: iterator i2;
    
    for (i = 0;i < SNAKE_INITIAL_BODY;i++)
    {
        ptr_body = new (BODY);
        
        ptr_body->x = x;
        ptr_body->y = y;
        
        aux_body = (*ptr_body);
        
        body.push_back (aux_body);
        
        x -= SNAKE_SQR_W * 2;
        
        //free (ptr_body);
    }
    
    al_load_font ("resource/Font/font01.ttf", 0, 2);
    
    createFood();
    
    direction = 1;
    spd       = SNAKE_SQR_W;
    
    pts = 0;
    
    if (!bck_music.open ("resource/Snd/snd01.wav"))
    {
        fprintf(stderr, "Failed to load Sound!\n");
        return -1;
    }
    
    bck_music.play (1.0, 1, true);
    
    return (1);
}

int Snake :: print()
{
    char                        msg[100];
    vector <BODY> :: iterator   i;
    
    strcpy (msg, "Pontos: ");
    strcat (msg, toString(pts));
    
    al_draw_text(font, SNAKE_COLOR, 0, 0, 0, msg);
    
    al_draw_rectangle(
            0, 0, 
            SCREEN_W, SCREEN_H, 
            SNAKE_COLOR, 2);
    
    for (i = body.begin();i != body.end();i++)
    {
        al_draw_filled_rectangle(
            i->x, i->y, 
            i->x + SNAKE_SQR_W, i->y + SNAKE_SQR_H, 
            SNAKE_COLOR);
    }
    
    al_draw_filled_rectangle(
            foodX, foodY, 
            foodX + FOOD_W, foodY + FOOD_H, 
            FOOD_COLOR);
    
    return (1);
}

int Snake :: move()
{
    short int                   aux;
    static int                  ate = 0;
    
    vector <BODY> :: iterator   i, i2;
    
    BODY        *ptr_body, 
                 aux_body;
    
    for (i = body.end()-2, i2 = body.end()-1;i != body.begin();i--, i2--)
    {
        if(ate == 1 && i2 == body.end()-1)
            ate = 0;
            
        else
            (*i2) = (*i);
    }
        
    i = body.begin();
    (*i2) = (*i);
    
    switch (direction)
    {
           case 0:/* L */
               i->x -= spd;
               break;
               
           case 1:/* R */
               i->x += spd;
               break;
               
           case 2:/* U */
               i->y -= spd;
               break;
               
           case 3:/* D */
               i->y += spd;
               break;
    }
    
    if (i->x < 0 || i->x + SNAKE_SQR_W > SCREEN_W ||
        i->y < 0 || i->y + SNAKE_SQR_H > SCREEN_H)
           GameOver();
    
    else if( (i->x >= foodX && i->x <= foodX + FOOD_W &&
        i->y >= foodY && i->y <= foodY + FOOD_H) ||
        
        (i->x + SNAKE_SQR_W >= foodX && i->x + SNAKE_SQR_W <= foodX + FOOD_W &&
        i->y >= foodY && i->y <= foodY + FOOD_H) )
    {
        for(aux = 0;aux < FOOD_GAIN;aux++)
        {
            i = body.end() - 1;
         
            ptr_body = new (BODY);
        
            ptr_body->x = i->x;
            ptr_body->y = i->y;
         
            aux_body = (*ptr_body);
            body.push_back (aux_body);
        }
         
         ate = 1;
         
         createFood();
         
         pts++;
    }
    
    else
    {
        /*
        i2 = body.begin();
        for (i = body.begin() + 1;i != body.end();i++)
        {
            if (i2->x >= i->x && i2->x <= i->x + SNAKE_SQR_W)
            {
                if (i2->y >= i->y && i2->y <= i->y + SNAKE_SQR_H)
                    GameOver();
            }
        }*/
    }
    
    return (1);
}

#endif
